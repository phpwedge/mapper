[![Codacy Badge](https://app.codacy.com/project/badge/Grade/f0126a267081493996a9e0ad35831d99)](https://app.codacy.com/bb/phpwedge/mapper/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/f0126a267081493996a9e0ad35831d99)](https://app.codacy.com/bb/phpwedge/mapper/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_coverage)
[![Latest Stable Version](https://poser.pugx.org/phpwedge/mapper/v/stable)](https://packagist.org/packages/phpwedge/mapper)
[![Total Downloads](https://poser.pugx.org/phpwedge/mapper/downloads)](https://packagist.org/packages/phpwedge/mapper)
[![PHP Version Require](http://poser.pugx.org/phpwedge/text/require/php)](https://packagist.org/packages/phpwedge/mapper)
[![License](https://poser.pugx.org/phpwedge/mapper/license)](https://packagist.org/packages/phpwedge/mapper)

# PhpWedge Entity-Mapper Package
>This package contains Entity and Mapper abstract classes for entity and entity mapping use cases.
### How to setup?
#### via composer cli
````bash
composer require phpwedge/mapper
````
#### via composer.json
````json
  "require": {
    "phpwedge/mapper": "^1.0.0"
  }
````
### How to use?
#### Entity example
##### Class declaration
````php
<?php
use PhpWedge\Core\Entity\EntityAbstract;

class MyEntity extends EntityAbstract {
    /** @var string */
    private $myProperty;
    
    public function getMyProperty(): string
    {
        return $this->myProperty;
    }
    
    public function setMyProperty(string $property)
    {
        $this->myProperty = $property;
    }
}
````
##### Usage
````php
<?php
$entity = new MyEntity();
$entity->setMyProperty('abc');
$json = json_encode($entity);
````
#### Mapper example
##### Class declaration
````php
<?php
use PhpWedge\Core\Mapper\MapperAbstract;

class MyMapper extends MapperAbstract {
    protected function getEntity(): EntityInterface 
    {
        return new MyEntity();
    }
    
}
````
##### Usage
````php
<?php
$entries = ['myProperty' => 'abcd'];
$mapper  = new MyMapper();
$entity  = $mapper->map($entries);
````

#### JsonSerializable traits

The JsonSerializable traits make sure that the entity is json serializable for different kind of formats.  
  
The possible formats are:  

- **JsonSerializableTrait**: as it is
- **JsonSerializableCamelCaseTrait**: changes the property names to camelCase
- **JsonSerializablePascalCaseTrait**: changes the property names to PascalCase
- **JsonSerializableSnakeCaseTrait**: changes the property names to snake_case
- **JsonSerializableKebabCaseTrait**: changes the property names to kebab-case
- **JsonSerializableShoutingSnakeCaseTrait**: changes the property names to SHOUTING_SNAKE_CASE
- **JsonSerializableShoutingKebabCaseTrait**: changes the property names to SHOUTING_KEBAB_CASE

##### Class declaration
````php
<?php
use PhpWedge\Core\Entity\JsonSerializableTrait;

class MyEntity {
    use JsonSerializableTrait;

    private $myProperty = 'abc';
}
````

##### Usage
````php
<?php
$entity  = new MyEntity();
$json = json_encode($entity); // {"myProperty":"abc"}
````
