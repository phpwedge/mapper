<?php

namespace PhpWedgeTest\Core\JsonSerializable\Fixture;

use JsonSerializable;
use PhpWedge\Core\JsonSerializable\JsonSerializableCamelCaseTrait;

class FakeMainEntity implements JsonSerializable
{
    use JsonSerializableCamelCaseTrait;

    private $name;
    private $fakeEntity;
    private $fakeEntities = [];

    public function __construct()
    {
        $this->name = 'NAME';
        $this->fakeEntity = new FakeEntity();
        $this->fakeEntities[] = new FakeEntity();
        $this->fakeEntities[] = new FakeEntity();
    }
}
