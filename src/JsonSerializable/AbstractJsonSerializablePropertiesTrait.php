<?php
/**
 * MIT License
 *
 * Copyright (c) 2023 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PhpWedge\Core\JsonSerializable;

use JsonSerializable;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

trait AbstractJsonSerializablePropertiesTrait
{
    /**
     * Returns the array representation of the field.
     *
     * @return array
     *
     * @throws ReflectionException
     */
    public function jsonSerialize(): array
    {
        return $this->getHandleElements(
            (new ReflectionClass($this))->getProperties()
        );
    }

    protected function getHandleElements(array $elements): array
    {
        $handledElements = [];
        foreach ($elements as $key => $value) {
            $this->handleElement($key, $value, $handledElements);
        }

        return $handledElements;
    }

    protected function handleElement($key, $value, array &$handledElements): void
    {
        if ($value instanceof ReflectionProperty) {
            $this->handleProperty($value, $handledElements);
        } elseif ($value instanceof JsonSerializable) {
            $handledElements[$key] = $value->jsonSerialize();
        } elseif (is_array($value)) {
            $handledElements[$key] = $this->getHandleElements($value);
        } else {
            $handledElements[$key] = $value;
        }
    }

    protected function handleProperty(ReflectionProperty $property, array &$handledElements): void
    {
        $property->setAccessible(true);
        if ($property->isInitialized($this)) {
            $propertyValue = $property->getValue($this) instanceof JsonSerializable
                ? $property->getValue($this)->jsonSerialize()
                : $property->getValue($this);
            if ($propertyValue !== null) {
                $handledElements[$this->getConvertedPropertyName($property->getName())] = is_array($propertyValue)
                    ? $this->getHandleElements($propertyValue)
                    : $propertyValue;
            }
        }
    }
}
