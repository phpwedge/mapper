<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PhpWedge\Core\Mapper;


use PhpWedge\Core\Entity\EntityInterface;

abstract class MapperAbstract implements MapperInterface
{
    /**
     * Maps the given entries into the entity.
     *
     * @param array $entries The entries to map.
     *
     * @return EntityInterface   The mapped entity.
     *
     * @throws MapperException   When the mappable method does not exist.
     */
    public function map(array $entries): EntityInterface
    {
        $entity = $this->getEntity();

        foreach ($entries as $key => $value) {
            $method = 'set' . ucfirst($key);

            if (!method_exists($entity, $method)) {
                throw new MapperException('The given method(' . $method . ') is unavailable!');
            }

            $entity->$method($value);
        }

        return $entity;
    }

    /**
     * Returns the entity for mapping into.
     *
     * @return EntityInterface
     */
    abstract protected function getEntity(): EntityInterface;
}
